import React, {Component} from 'react';
import './button.scss'

class Button extends Component {
  render() {
    const { innerText, onClick, dataId, color } = this.props
    return (
      <>
        <button data-id={dataId} className="custom-btn" onClick={onClick} style={{background: color}}>{innerText}</button>
      </>
    );
  }
}

export default Button;