import React from 'react';
import './style.scss'
import Modal from './modal/Modal';
import Button from "./button/Button";
import config from './config/config'

class App extends React.Component {
  state ={
    active: false,
  }
  changeActive = () => this.setState({active: !this.state.active})

  showModal = (event) => {
    const modalId = event.target.dataset.id
    const modalDeclaration = config.find( item => item.id === modalId)
    this.setState({
      active: !this.state.active,
      ...modalDeclaration
    })
  }

  render(){
    const { active, header, description, action, backgroundColor } = this.state
    return (
      <>
        <Button dataId="modalID1" onClick={this.showModal} color="#4E6642FF" innerText="Open first modal"/>
        <Button dataId="modalID2" onClick={this.showModal} color="#861F1FFF" innerText="Open second modal"/>
        {this.state.active &&
          <Modal
            active={active}
            changeActive={this.changeActive}
            header={header}
            description={description}
            action={action}
            backgroundColor={backgroundColor}
          />}
      </>
    )
  }
}

export default App;
