const configInfo = [
  {
    id: 'modalID1',
    header: "First modal",
    description: 'React (also known as React.js or ReactJS) is a free and open-source front-end JavaScript library' +
      ' for building user interfaces based on UI components. It is maintained by Meta (formerly Facebook) and a ' +
      'community of individual developers and companies. React can be used as a base in the development ' +
      'of single-page, mobile',
    backgroundColor: 'rgb(207,255,188)',
    action: (
      <>
        <button className="custom-btn green">Ok</button>
        <button className="custom-btn red">Cancel</button>
      </>
    )
  },
  {
    id: 'modalID2',
    header: "Second modal",
    description: 'Vue is a JavaScript framework for building user interfaces. ' +
      'It builds on top of standard HTML, CSS and JavaScript, and provides a declarative and component-based ' +
      'programming model that helps you efficiently develop user interfaces, be it simple or complex.',
    backgroundColor: '#db4b4b',
    action: (
      <>
        <button className="custom-btn green">Ok</button>
        <button className="custom-btn red">Cancel</button>
      </>
    )
  }
]

export default configInfo