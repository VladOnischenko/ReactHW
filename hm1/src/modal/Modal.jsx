import React from 'react'
import './modal.scss'

class Modal extends React.Component {
  handlerModal = () => this.props.active ? 'modal active' : 'modal'

  render() {
    const { changeActive, active, description, header, action, backgroundColor } = this.props
    return (
      <div className={this.handlerModal()} onClick={changeActive}>
        <div className={ active ? "modal__container active" : "modal__container"} style={{background: backgroundColor}} onClick={(e) => e.stopPropagation()}>
          <div className="modal__header">
            <h3>{header}</h3>
            <button onClick={changeActive} className="modal__close-btn">&times;</button>
          </div>
          <p className="modal__body">
            {description}
          </p>
          <div className="modal__footer">
            {action}
          </div>
        </div>
      </div>
    )
  }
}

export default Modal